<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Contracts\Role as RoleContract;

class CreateSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'super-admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->ask('What is your user name?');
        $email = $this->ask('What is your email address?');

        list($password, $passwordConfirm) = $this->askPassword();

        while ($password !== $passwordConfirm) {
            $this->alert('Passwords do not match. Try again.');
            list($password, $passwordConfirm) = $this->askPassword();

        }

        $user = User::firstOrCreate([
            'name' => $name
        ], [
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        app(RoleContract::class)->firstOrCreate([
            'name' => 'super-admin'
        ], [
            'guard_name' => 'web'
        ]);

        $user->assignRole('super-admin');

        $this->info('The admin user was successful created or updated!');

        return 0;
    }

    private function askPassword()
    {
        return [
            $this->secret('Enter a password'),
            $this->secret('Re-enter your password')
        ];
    }
}
