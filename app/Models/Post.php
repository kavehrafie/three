<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, Sluggable;

    protected $casts = [
        'published' => 'boolean',
        'content' => 'array',
        'start' => 'date',
        'end' => 'date'
    ];

    protected $fillable = [
        'title',
        'start',
        'end',
        'published',
        'content',
        'type_id',
        'abstract'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function type()
    {
        return $this->belongsTo(PostType::class,);
    }

    public function blocks()
    {
        return $this->morphMany(Block::class, 'blockable');
    }
}
