// initial state
const state = () => ({
    all: [],
    count: 0
})

// getters
const getters = {}

// actions
const actions = {
    // async getAllProducts ({ commit }) {
    //     const products = await shop.getProducts()
    //     commit('setProducts', products)
    // }
}

// mutations
const mutations = {
    increment(state) {
        state.count++
    }
    // setProducts (state, products) {
    //     state.all = products
    // },
    //
    // decrementProductInventory (state, { id }) {
    //     const product = state.all.find(product => product.id === id)
    //     product.inventory--
    // }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
