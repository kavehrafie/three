import {createLogger, createStore} from "vuex";
import medialibrary from "./modules/medialibrary";

const debug = process.env.NODE_ENV !== 'production'

export default createStore({
    modules: {
        medialibrary,
    },
    strict: debug,
    plugins: debug ? [createLogger()] : []
})
