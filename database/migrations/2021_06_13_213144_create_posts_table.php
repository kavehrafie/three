<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_types', function(Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title', 250);
            $table->string('slug');
            $table->boolean('published')->default(false);
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->string('abstract', 500)->nullable();
            $table->foreignId('type_id')->nullable()->constrained()->on('post_types')->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_types');
        Schema::dropIfExists('posts');
    }
}
