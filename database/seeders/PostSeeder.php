<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\PostType;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostType::upsert([
            ['name' => 'type1'],
            ['name' => 'type2'],
            ['name' => 'type3']
        ], ['name'], ['name']);

        Post::factory()->count(5)->create();
    }
}
