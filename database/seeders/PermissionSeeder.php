<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Contracts\Permission as PermissionContract;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissionNameList = collect([
            'create post',
            'edit post',
            'delete post',
            'show post',
            'edit user',
            'show user',
            'delete user',
            'create user'])
            ->map( fn($name) => ['name' => $name ])
            ->toArray();

        foreach($permissionNameList as $permissionName) {
            app()[PermissionContract::class]->firstOrCreate($permissionName);
        }

    }
}
