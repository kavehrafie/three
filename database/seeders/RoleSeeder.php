<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Contracts\Permission as PermissionContract;
use Spatie\Permission\Contracts\Role as RoleContract;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleClass = app(RoleContract::class);
        $permissionClass = app(PermissionContract::class);

        if($this->permissionsIsEmpty()) {
            $this->callPermissionSeeder();
        }

        $roles = [
            'editor' => [
                'create post',
                'edit post',
                'delete post',
                'show post',
                'show user',
            ],
            'guest' => [
                'show post'
            ]];

        foreach($roles as $roleName => $permissionNames) {
            $role = $roleClass::firstOrCreate(['name' => $roleName]);

            $role->syncPermissions($permissionNames);
        }

        $this->command->info("Creating sample users");
        $this->createSampleUsers();

        $this->command->info("Roles are successfully created!");
    }

    private function permissionsIsEmpty()
    {
        $permissionClass = app(PermissionContract::class);

        $permissions = [
            'create post',
            'edit post',
            'delete post',
            'show post',
            'edit user',
            'show user',
            'delete user',
            'create user'
        ];
        return $permissionClass::whereIn('name', $permissions)->get()->isEmpty();
    }

    private function callPermissionSeeder(): void
    {
        $this->command->info('First run Permission Seeder');
        $this->call([
            PermissionSeeder::class
        ]);
    }

    private function createSampleUsers()
    {
        $roles = app()[RoleContract::class]->all();

        foreach ($roles as $role) {
            $user = User::factory()->make([
                'name' => $role->name,
            ]);
            $user->assignRole($role);
        }
    }
}
