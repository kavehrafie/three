<?php

use App\Http\Controllers\Admin\PostController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


Route::get('/dashboard', function () {
    return Inertia::render('Dashboard', [
        'can' => [
            'test_can' => auth()->user()->can('edit post')
        ]
    ]);
})->name('dashboard');

Route::resource('posts', PostController::class);
